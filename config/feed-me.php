<?php

return [
    '*' => [
        'pluginName' => 'Feed Me',
        'cache' => 60,
        'requestOptions' => [],
        'skipUpdateFieldHandle' => 'skipFeedMeUpdate',
        'backupLimit' => 100,
        'dataDelimiter' => '-|-',
        'csvColumnDelimiter' => '',
        'parseTwig' => false,
        'compareContent' => true,
        'sleepTime' => 0,
        'logging' => true,
        'runGcBeforeFeed' => false,
        'queueTtr' => 300,
        'queueMaxRetry' => 5,
        'assetDownloadCurl' => false,
        'feedOptions' => [
            '1' => [
                'feedUrl' => '../storage/json/classes.json',
                'requestOptions' => [],
            ],
            '2' => [
                'feedUrl' => '../storage/json/courses.json',
                'requestOptions' => [],
            ],
            '3' => [
                'feedUrl' => '../storage/json/links.xml',
                'requestOptions' => [],
            ],
            '4' => [
                'feedUrl' => '../storage/json/menus.json',
                'requestOptions' => [],
            ],
        ],
    ]
];
