<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130113_migration_entry_main_menu_admissions_quick_links_academics_quick_links_credits_standing_quick_links_disability_services_quick_links extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - main-menu
    - admissions-quick-links
    - academics-quick-links
    - credits-standing-quick-links
    - disability-services-quick-links
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"main-menu","section":"menus","sites":{"default":{"slug":"main-menu","section":"menus","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-12-21 12:11:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Main Menu","entryType":"default","uid":"e4dcc1b1-809b-442a-a2cc-88e66ff7f635","author":"admin","fields":{"linkEntries":[]}}}},{"slug":"admissions-quick-links","section":"menus","sites":{"default":{"slug":"admissions-quick-links","section":"menus","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-12-21 13:22:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Admissions - Quick Links","entryType":"default","uid":"43941002-573d-4dab-8014-61ab5748ea2b","author":"admin","fields":{"linkEntries":[]}}}},{"slug":"academics-quick-links","section":"menus","sites":{"default":{"slug":"academics-quick-links","section":"menus","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-12-21 14:57:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Academics - Quick Links","entryType":"default","uid":"5e537b17-e0fc-4e82-94fc-4d027c2213b0","author":"admin","fields":{"linkEntries":[]}}}},{"slug":"credits-standing-quick-links","section":"menus","sites":{"default":{"slug":"credits-standing-quick-links","section":"menus","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-12-21 15:30:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Credits & Standing - Quick Links","entryType":"default","uid":"39ac6a9f-7a7e-464c-9f8a-3f21fd3acc79","author":"admin","fields":{"linkEntries":[]}}}},{"slug":"disability-services-quick-links","section":"menus","sites":{"default":{"slug":"disability-services-quick-links","section":"menus","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-12-21 15:35:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Disability Services - Quick Links","entryType":"default","uid":"1766066f-e04c-4294-aba1-250b7671e573","author":"admin","fields":{"linkEntries":[]}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130113_migration_entry_main_menu_admissions_quick_links_academics_quick_links_credits_standing_quick_links_disability_services_quick_links cannot be reverted.\n";
        return false;
    }
}
