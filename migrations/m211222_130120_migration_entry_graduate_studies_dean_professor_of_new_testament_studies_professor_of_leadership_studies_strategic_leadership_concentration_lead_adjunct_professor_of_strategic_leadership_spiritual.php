<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130120_migration_entry_graduate_studies_dean_professor_of_new_testament_studies_professor_of_leadership_studies_strategic_leadership_concentration_lead_adjunct_professor_of_strategic_leadership_spiritual extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - graduate-studies-dean
    - professor-of-new-testament-studies
    - professor-of-leadership-studies
    - strategic-leadership-concentration-lead
    - adjunct-professor-of-strategic-leadership
    - spiritual-formation-concentration-lead
    - professor-of-spiritual-formation
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"graduate-studies-dean","section":"roleTitles","sites":{"default":{"slug":"graduate-studies-dean","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:02:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Graduate Studies Dean","entryType":"default","uid":"b2a7876b-786e-436d-bb76-6e2f482c5f4a","author":"admin"}}},{"slug":"professor-of-new-testament-studies","section":"roleTitles","sites":{"default":{"slug":"professor-of-new-testament-studies","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:02:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Professor of New Testament Studies","entryType":"default","uid":"62b1ab1c-f0c3-4b9c-acd9-b1428cbaa8a8","author":"admin"}}},{"slug":"professor-of-leadership-studies","section":"roleTitles","sites":{"default":{"slug":"professor-of-leadership-studies","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:03:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Professor of Leadership Studies","entryType":"default","uid":"89d6e821-1515-4d61-b1ad-14cb533001ba","author":"admin"}}},{"slug":"strategic-leadership-concentration-lead","section":"roleTitles","sites":{"default":{"slug":"strategic-leadership-concentration-lead","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:03:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Strategic Leadership Concentration Lead","entryType":"default","uid":"1952e2fd-2d98-405a-9283-26bab84f44fb","author":"admin"}}},{"slug":"adjunct-professor-of-strategic-leadership","section":"roleTitles","sites":{"default":{"slug":"adjunct-professor-of-strategic-leadership","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:03:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Adjunct Professor of Strategic Leadership","entryType":"default","uid":"945299f9-d341-42c0-b259-f105e9085d86","author":"admin"}}},{"slug":"spiritual-formation-concentration-lead","section":"roleTitles","sites":{"default":{"slug":"spiritual-formation-concentration-lead","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:03:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Spiritual Formation Concentration Lead","entryType":"default","uid":"9939b213-b571-432f-b108-0cae41f92d10","author":"admin"}}},{"slug":"professor-of-spiritual-formation","section":"roleTitles","sites":{"default":{"slug":"professor-of-spiritual-formation","section":"roleTitles","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:04:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Professor of Spiritual Formation","entryType":"default","uid":"1e923c52-1e92-4b7f-955b-708ef9fc3b84","author":"admin"}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130120_migration_entry_graduate_studies_dean_professor_of_new_testament_studies_professor_of_leadership_studies_strategic_leadership_concentration_lead_adjunct_professor_of_strategic_leadership_spiritual cannot be reverted.\n";
        return false;
    }
}
