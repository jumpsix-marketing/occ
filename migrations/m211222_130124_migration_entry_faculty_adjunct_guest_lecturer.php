<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130124_migration_entry_faculty_adjunct_guest_lecturer extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - faculty
    - adjunct
    - guest-lecturer
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"faculty","section":"roleTypeEntries","sites":{"default":{"slug":"faculty","section":"roleTypeEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:30:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Faculty","entryType":"default","uid":"be7b2f63-d3cb-4cd9-bf19-790406db7ec1","author":"admin"}}},{"slug":"adjunct","section":"roleTypeEntries","sites":{"default":{"slug":"adjunct","section":"roleTypeEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:31:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Adjunct","entryType":"default","uid":"4cb8edc7-86ba-4083-8811-fa5455471d5e","author":"admin"}}},{"slug":"guest-lecturer","section":"roleTypeEntries","sites":{"default":{"slug":"guest-lecturer","section":"roleTypeEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 19:31:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Guest Lecturer","entryType":"default","uid":"d9cbf9ff-75c8-4533-946c-fcfc981e1251","author":"admin"}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130124_migration_entry_faculty_adjunct_guest_lecturer cannot be reverted.\n";
        return false;
    }
}
