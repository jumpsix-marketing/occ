<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130133_migration_entry_biblical_studies_department_spiritual_formation_department_biblical_ministry_department_strategic_leadership_department extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - biblical-studies-department
    - spiritual-formation-department
    - biblical-ministry-department
    - strategic-leadership-department
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"biblical-studies-department","section":"departmentEntries","sites":{"default":{"slug":"biblical-studies-department","section":"departmentEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 18:57:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Biblical Studies","entryType":"default","uid":"ecd34657-24d4-4572-b32d-89747caec802","author":"admin","fields":{"departmentName":"Biblical Studies"}}}},{"slug":"spiritual-formation-department","section":"departmentEntries","sites":{"default":{"slug":"spiritual-formation-department","section":"departmentEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 18:57:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Spiritual Formation","entryType":"default","uid":"9ff39021-c7c7-477b-81ce-3080857e8025","author":"admin","fields":{"departmentName":"Spiritual Formation"}}}},{"slug":"biblical-ministry-department","section":"departmentEntries","sites":{"default":{"slug":"biblical-ministry-department","section":"departmentEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 18:58:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Biblical Ministry","entryType":"default","uid":"381019fa-63d2-40ee-9256-31158cedeb06","author":"admin","fields":{"departmentName":"Biblical Ministry"}}}},{"slug":"strategic-leadership-department","section":"departmentEntries","sites":{"default":{"slug":"strategic-leadership-department","section":"departmentEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-17 18:58:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Strategic Leadership","entryType":"default","uid":"bc2ecb87-e271-464b-b3b4-9aa7ca45465d","author":"admin","fields":{"departmentName":"Strategic Leadership"}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130133_migration_entry_biblical_studies_department_spiritual_formation_department_biblical_ministry_department_strategic_leadership_department cannot be reverted.\n";
        return false;
    }
}
