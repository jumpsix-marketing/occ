<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130409_migration_entry_fall_2021_dates_orientation_term_begins_interactive_intensive_week_thanksgiving_break_term_begins_2 extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - fall-2021-dates
    - orientation
    - term-begins
    - interactive-intensive-week
    - thanksgiving-break
    - term-begins-2
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"fall-2021-dates","section":"calendarEntries","sites":{"default":{"slug":"fall-2021-dates","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:13:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Fall 2021 Dates","entryType":"default","uid":"59b9f063-2d5e-4698-8acb-1b1889d1d9bf","author":"admin","fields":{"altText":null}}}},{"slug":"orientation","section":"calendarEntries","sites":{"default":{"slug":"orientation","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:13:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Orientation","entryType":"default","uid":"a6b50839-3728-4f40-93eb-3135834f3326","author":"admin","fields":{"altText":"Aug. 8–12"}}}},{"slug":"term-begins","section":"calendarEntries","sites":{"default":{"slug":"term-begins","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:14:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Term Begins","entryType":"default","uid":"8991349c-80d4-4d0c-937b-66673c489d89","author":"admin","fields":{"altText":"Sept. 5th"}}}},{"slug":"interactive-intensive-week","section":"calendarEntries","sites":{"default":{"slug":"interactive-intensive-week","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:14:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Interactive Intensive Week","entryType":"default","uid":"3965f23f-a741-4cc3-8c34-2735dc8d3d7b","author":"admin","fields":{"altText":"Oct. 17–21"}}}},{"slug":"thanksgiving-break","section":"calendarEntries","sites":{"default":{"slug":"thanksgiving-break","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:14:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Thanksgiving Break","entryType":"default","uid":"23486bc4-4413-40ef-ac0f-ec8d36ea0ccf","author":"admin","fields":{"altText":"Nov. 21–27"}}}},{"slug":"term-begins-2","section":"calendarEntries","sites":{"default":{"slug":"term-begins-2","section":"calendarEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-15 11:15:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Term Begins","entryType":"default","uid":"9bcabe55-e50f-407f-9d51-a47fcc9a186c","author":"admin","fields":{"altText":"Dec. 9th"}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130409_migration_entry_fall_2021_dates_orientation_term_begins_interactive_intensive_week_thanksgiving_break_term_begins_2 cannot be reverted.\n";
        return false;
    }
}
