<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130639_migration_entry_shane_j_wood_wade_landers_chris_dewelt_alicia_crumpton_doug_welch extends Migration
{
    /**
    Migration manifest:

    ENTRY
    - shane-j-wood
    - wade-landers
    - chris-dewelt
    - alicia-crumpton
    - doug-welch
    */

    private $json = <<<'JSON'
{"content":{"entries":[{"slug":"shane-j-wood","section":"facultyEntries","sites":{"default":{"slug":"shane-j-wood","section":"facultyEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-08 17:19:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Shane J. Wood","entryType":"default","uid":"0b675e76-8b96-47c6-a121-d4ff81e3f83f","author":"admin","fields":{"headingText":"Graduate Studies Dean / Professor of New Testament Studies","altText":"Ph.D., University of Edinburgh (UK)","departmentEntries":[{"elementType":"craft\\elements\\Entry","slug":"biblical-studies-department","section":"departmentEntries","site":"default"}],"roleTypeEntries":[{"elementType":"craft\\elements\\Entry","slug":"faculty","section":"roleTypeEntries","site":"default"}],"roleTitleEntries":[{"elementType":"craft\\elements\\Entry","slug":"graduate-studies-dean","section":"roleTitles","site":"default"},{"elementType":"craft\\elements\\Entry","slug":"professor-of-new-testament-studies","section":"roleTitles","site":"default"}],"email":"wood.shane@occ.edu","phone":"417-626-1291","facultyImages":[{"elementType":"craft\\elements\\Asset","filename":"Shane_Wood_2021.png","folder":"Faculty","source":"faculty","path":""}]}}}},{"slug":"wade-landers","section":"facultyEntries","sites":{"default":{"slug":"wade-landers","section":"facultyEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-08 17:33:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Wade Landers","entryType":"default","uid":"ac1ca81d-2728-4014-b544-fd1ecf10be36","author":"admin","fields":{"headingText":"Professor of Leadership Studies","altText":"Ph.D. (candidate), Biola University","departmentEntries":[{"elementType":"craft\\elements\\Entry","slug":"spiritual-formation-department","section":"departmentEntries","site":"default"}],"roleTypeEntries":[{"elementType":"craft\\elements\\Entry","slug":"faculty","section":"roleTypeEntries","site":"default"}],"roleTitleEntries":[{"elementType":"craft\\elements\\Entry","slug":"professor-of-leadership-studies","section":"roleTitles","site":"default"}],"email":"landers.wade@occ.edu","phone":null,"facultyImages":[{"elementType":"craft\\elements\\Asset","filename":"Wade_Landers_2017_-_smaller.jpg","folder":"Faculty","source":"faculty","path":""}]}}}},{"slug":"chris-dewelt","section":"facultyEntries","sites":{"default":{"slug":"chris-dewelt","section":"facultyEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-08 17:37:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Chris Dewelt","entryType":"default","uid":"7ad9e017-dfbf-4894-857d-83c16d6447c0","author":"admin","fields":{"headingText":"Strategic Leadership Concentration Lead / Professor of Leadership Studies","altText":"D. Miss., Biola University","departmentEntries":[{"elementType":"craft\\elements\\Entry","slug":"biblical-ministry-department","section":"departmentEntries","site":"default"}],"roleTypeEntries":[{"elementType":"craft\\elements\\Entry","slug":"faculty","section":"roleTypeEntries","site":"default"}],"roleTitleEntries":[{"elementType":"craft\\elements\\Entry","slug":"strategic-leadership-concentration-lead","section":"roleTitles","site":"default"},{"elementType":"craft\\elements\\Entry","slug":"professor-of-leadership-studies","section":"roleTitles","site":"default"}],"email":"dewelt.chris@occ.edu","phone":"417-680-5618","facultyImages":[{"elementType":"craft\\elements\\Asset","filename":"Chris_DeWelt_2016.png","folder":"Faculty","source":"faculty","path":""}]}}}},{"slug":"alicia-crumpton","section":"facultyEntries","sites":{"default":{"slug":"alicia-crumpton","section":"facultyEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-08 17:41:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Alicia Crumpton","entryType":"default","uid":"f79b5cc1-48f8-4077-baca-e95ee9f15a63","author":"admin","fields":{"headingText":"Adjunct Professor of Strategic Leadership","altText":"Ph.D. in Leadership Studies, Gonzaga University","departmentEntries":[{"elementType":"craft\\elements\\Entry","slug":"strategic-leadership-department","section":"departmentEntries","site":"default"}],"roleTypeEntries":[{"elementType":"craft\\elements\\Entry","slug":"adjunct","section":"roleTypeEntries","site":"default"}],"roleTitleEntries":[{"elementType":"craft\\elements\\Entry","slug":"adjunct-professor-of-strategic-leadership","section":"roleTitles","site":"default"}],"email":null,"phone":null,"facultyImages":[{"elementType":"craft\\elements\\Asset","filename":"Generic-Profile-Placeholder-v3.png","folder":"Faculty","source":"faculty","path":""}]}}}},{"slug":"doug-welch","section":"facultyEntries","sites":{"default":{"slug":"doug-welch","section":"facultyEntries","enabled":true,"site":"default","enabledForSite":true,"postDate":{"date":"2021-11-08 17:46:00.000000","timezone_type":3,"timezone":"America/Chicago"},"expiryDate":null,"title":"Doug Welch","entryType":"default","uid":"b18c3c61-8912-490d-873e-867b041107f2","author":"admin","fields":{"headingText":"Spiritual Formation Concentration Lead / Professor of Spiritual Formation","altText":"D. Min. (candidate), Northern Seminary","departmentEntries":[],"roleTypeEntries":[{"elementType":"craft\\elements\\Entry","slug":"faculty","section":"roleTypeEntries","site":"default"}],"roleTitleEntries":[{"elementType":"craft\\elements\\Entry","slug":"spiritual-formation-concentration-lead","section":"roleTitles","site":"default"},{"elementType":"craft\\elements\\Entry","slug":"professor-of-spiritual-formation","section":"roleTitles","site":"default"}],"email":"welch.doug@occ.edu","phone":"417-680-5625","facultyImages":[{"elementType":"craft\\elements\\Asset","filename":"Doug_Welch_2019.png","folder":"Faculty","source":"faculty","path":""}]}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130639_migration_entry_shane_j_wood_wade_landers_chris_dewelt_alicia_crumpton_doug_welch cannot be reverted.\n";
        return false;
    }
}
