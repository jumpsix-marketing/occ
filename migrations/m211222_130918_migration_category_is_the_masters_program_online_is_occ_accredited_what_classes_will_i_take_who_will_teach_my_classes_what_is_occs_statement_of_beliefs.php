<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130918_migration_category_is_the_masters_program_online_is_occ_accredited_what_classes_will_i_take_who_will_teach_my_classes_what_is_occs_statement_of_beliefs extends Migration
{
    /**
    Migration manifest:

    CATEGORY
    - is-the-masters-program-online
    - is-occ-accredited
    - what-classes-will-i-take
    - who-will-teach-my-classes
    - what-is-occs-statement-of-beliefs
    */

    private $json = <<<'JSON'
{"content":{"categories":[{"slug":"is-the-masters-program-online","category":"faqs","sites":{"default":{"slug":"is-the-masters-program-online","category":"faqs","enabled":true,"site":"default","enabledForSite":true,"title":"Is the master\u0027s program online?","uid":"fe29cfd6-07e5-4c02-a902-be5814988cd9","fields":{"bodyText":"<p>Yes, the program is offered in an interactive online format, so you can earn your entire degree without ever traveling from your location.</p>"}}}},{"slug":"is-occ-accredited","category":"faqs","sites":{"default":{"slug":"is-occ-accredited","category":"faqs","enabled":true,"site":"default","enabledForSite":true,"title":"Is OCC accredited?","uid":"b00de33b-f7ce-4efb-921d-385cf709b7fa","fields":{"bodyText":"<p>Yes, Ozark Christian College holds dual accreditation with the Higher\n Learning Commission (HLC) and with the Association of Biblical Higher \nEducation (ABHE).</p>"}}}},{"slug":"what-classes-will-i-take","category":"faqs","sites":{"default":{"slug":"what-classes-will-i-take","category":"faqs","enabled":true,"site":"default","enabledForSite":true,"title":"What classes will I take?","uid":"6767aac6-0351-4b67-b8c0-0c53b296a0f5","fields":{"bodyText":"<p>The Master of Arts in Biblical Ministry includes three degree \nconcentrations. Courses in your concentration include Contextualized \nHermeneutics, Christian Doctrine and Theology, Worldview and Cultural \nDiscernment, Biblical Theology of Mission, and Spiritual Formation and \nDiscipleship.</p>"}}}},{"slug":"who-will-teach-my-classes","category":"faqs","sites":{"default":{"slug":"who-will-teach-my-classes","category":"faqs","enabled":true,"site":"default","enabledForSite":true,"title":"Who will teach my classes?","uid":"6d061047-753f-4b85-bf02-bceff708bdba","fields":{"bodyText":"<p>Just like in our undergraduate program, Ozark’s master’s program \nfaculty set us apart. You’ll learn from practitioner professors—men and \nwomen with hands-on experience in their fields—in a distinctively \nChrist-centered community. </p>"}}}},{"slug":"what-is-occs-statement-of-beliefs","category":"faqs","sites":{"default":{"slug":"what-is-occs-statement-of-beliefs","category":"faqs","enabled":true,"site":"default","enabledForSite":true,"title":"What is OCC’s statement of beliefs?","uid":"fc7ed2ad-ac5f-4a8e-8cb2-88fa315f80b2","fields":{"bodyText":"<p dir=\u0022ltr\u0022><br /> Ozark Christian College has its roots in the \nStone-Campbell heritage (Independent Christian Churches and Churches of \nChrist) that began in the United States in the early 19th century. This \nheritage seeks the unity of all Christians based on the authority of the\n Bible for the evangelization of the world. OCC recognizes that creeds \nand confessions of faith have at times been more divisive than unifying,\n but in light of its commitment to Scripture, OCC believes that \nagreement on certain matters of the faith is essential to carry out its \nmission. Therefore to avoid any misunderstanding or misinterpretation, \nthe following statements are given and all trustees, administrators, and\n faculty affirm their unqualified acceptance of the following:</p>\n<p> <br /></p>\n<p><strong>GOD:</strong> </p>\n<p>There is one, holy God who eternally exists in three persons – \nFather, Son, and Holy Spirit. God created all things visible and \ninvisible. God is perfect in wisdom, power, and love, knowing all things\n past, present, and future, and his sovereign plan of redemption was set\n in place before the foundation of the world. </p>\n<p>(Gen 1:1-2, Dt. 6:4, Heb 11:3, Eph 1:9-10; Rev 13:8)<br /><br /></p>\n<p><strong>JESUS:</strong> </p>\n<p dir=\u0022ltr\u0022>Jesus Christ is God’s only begotten Son, born of a virgin, \nfully divine and fully human, and our Savior and Lord. Jesus, who was \nwithout sin, died in our place as a substitutionary sacrifice for our \nsins, bearing divine wrath, and reconciling to God all who trust in him.\n Jesus was bodily resurrected in victory over sin and death. He ascended\n to the right hand of the Father where he presently reigns as our king, \nhigh priest, and advocate until his glorious return. </p>\n<p dir=\u0022ltr\u0022>(John 3:16, Col 1:15; 2:9-15; 1 Cor 15:3-8, 20-28; 2 Cor 5:18-21; Heb 4:14-15)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />HOLY SPIRIT:</strong> </p>\n<p dir=\u0022ltr\u0022>The Holy Spirit is fully divine and active in the church \nand the world. The Holy Spirit draws all people to Christ by \nilluminating the gospel and convicting of sin. The Holy Spirit dwells in\n the life of a believer to transform, guide, assure, and empower living a\n fruitful Christian life. </p>\n<p dir=\u0022ltr\u0022>(John 16:8-11; Acts 2:38; 2 Cor 3:17-18; Gal 3:2)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />BIBLE:</strong> </p>\n<p dir=\u0022ltr\u0022>God is revealed in the Bible, the uniquely inspired written\n Word of God and infallible in all that it affirms. The Bible is the \nfinal authority in all matters of faith and practice. </p>\n<p dir=\u0022ltr\u0022>(2 Tim 3:16; 2 Pet 1:20-21)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />HUMANITY: </strong></p>\n<p dir=\u0022ltr\u0022>God creates all humans, male and female, in his image, and \ntherefore all people have intrinsic value and purpose. By the sin of the\n first man and woman (Adam and Eve), death entered the world. All have \nsinned and fall short of the glory of God, alienated from God and \nwithout hope apart from the blood of Jesus Christ. </p>\n<p dir=\u0022ltr\u0022>(Gen 1:26-27; Gen 3; Rom 3:23; Eph 2:1-3)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />SALVATION:</strong> </p>\n<p dir=\u0022ltr\u0022>Salvation can be found in Christ alone and is offered to \nall by grace through faith. A living faith is demonstrated through \nrepentance, confession, baptism by immersion, and a life of obedience. </p>\n<p dir=\u0022ltr\u0022>(Rom 3:23; 5:12, Acts 2:38, Gal 3:26-29; Eph 2:4-10)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />CHURCH:</strong> </p>\n<p dir=\u0022ltr\u0022>The church is the body of Christ on earth, with Christ as \nthe head. God’s church is comprised of a priesthood of all believers, \nserving as minister of the gospel according to the gifts which God has \ngiven them. Together the church is called to make disciples of all \nnations until Christ returns.</p>\n<p dir=\u0022ltr\u0022> (Matt 28:18-20; Eph 3:10; 4:11-13; Col 1:18; 1 Pet 2:9-10)<br /></p>\n<p dir=\u0022ltr\u0022><strong><br />RETURN OF CHRIST:</strong> </p>\n<p dir=\u0022ltr\u0022>Christ will visibly return to restore creation and judge \nthe world. There will be a bodily resurrection for the believers to \neternal life with God in heaven and for the unbelievers to eternal \njudgment in hell. In heaven, sin will be no more and those in Christ \nwill live in fellowship with God forever.</p>\n<p dir=\u0022ltr\u0022> (Acts 1:11; 2 Thess 1:5-12; 1 Thess 4:13-18; Rev 20:11-15)</p>"}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130918_migration_category_is_the_masters_program_online_is_occ_accredited_what_classes_will_i_take_who_will_teach_my_classes_what_is_occs_statement_of_beliefs cannot be reverted.\n";
        return false;
    }
}
