<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130924_migration_category_matthew_938_scholarship_revelation_79_diversity_and_international_scholarship_2_timothy_22_scholarship_austin_scholarship_mieir_occ_graduates_scholarship extends Migration
{
    /**
    Migration manifest:

    CATEGORY
    - matthew-938-scholarship
    - revelation-79-diversity-and-international-scholarship
    - 2-timothy-22-scholarship
    - austin-scholarship
    - mieir-occ-graduates-scholarship
    */

    private $json = <<<'JSON'
{"content":{"categories":[{"slug":"matthew-938-scholarship","category":"scholarships","sites":{"default":{"slug":"matthew-938-scholarship","category":"scholarships","enabled":true,"site":"default","enabledForSite":true,"title":"Matthew 9:38 Scholarship","uid":"a9f54ed9-2c0c-4038-8ff8-5290ab040944","fields":{"scholarships":{"new1":{"type":"2","fields":{"scholarshipName":"Matthew 9:38 Scholarship","awardAmount":"40% of tuition","qualifications":"Limited (need considered)","requirements":"GPA 3.5, references, essay, life experience"}}}}}}},{"slug":"revelation-79-diversity-and-international-scholarship","category":"scholarships","sites":{"default":{"slug":"revelation-79-diversity-and-international-scholarship","category":"scholarships","enabled":true,"site":"default","enabledForSite":true,"title":"Revelation 7:9 Diversity and International Scholarship","uid":"47149338-0d94-4cf6-a682-3439fb9f3ae3","fields":{"scholarships":{"new1":{"type":"2","fields":{"scholarshipName":"Revelation 7:9 Diversity and International Scholarship","awardAmount":"40% of tuition","qualifications":"Limited (need considered)","requirements":"GPA 3.0, references, essay, life experience"}}}}}}},{"slug":"2-timothy-22-scholarship","category":"scholarships","sites":{"default":{"slug":"2-timothy-22-scholarship","category":"scholarships","enabled":true,"site":"default","enabledForSite":true,"title":"2 Timothy 2:2 Scholarship","uid":"73f4b61a-300f-4524-9a33-99edb290b669","fields":{"scholarships":{"new1":{"type":"2","fields":{"scholarshipName":"2 Timothy 2:2 Scholarship","awardAmount":"25% of tuition","qualifications":"Limited (need considered)","requirements":"GPA 3.25, references, essay, life experience"}}}}}}},{"slug":"austin-scholarship","category":"scholarships","sites":{"default":{"slug":"austin-scholarship","category":"scholarships","enabled":true,"site":"default","enabledForSite":true,"title":"Austin Scholarship","uid":"f97e9c23-beec-4984-8688-84018d4b90b1","fields":{"scholarships":{"new1":{"type":"2","fields":{"scholarshipName":"Austin Scholarship","awardAmount":"10% of tuition","qualifications":"Limited (need considered)","requirements":"GPA 3.0, references, essay, life experience"}}}}}}},{"slug":"mieir-occ-graduates-scholarship","category":"scholarships","sites":{"default":{"slug":"mieir-occ-graduates-scholarship","category":"scholarships","enabled":true,"site":"default","enabledForSite":true,"title":"Mieir OCC Graduates Scholarship","uid":"be3fa9b5-f0c8-4c1d-9a9e-6f32cb6029bd","fields":{"scholarships":{"new1":{"type":"2","fields":{"scholarshipName":"Mieir OCC Graduates Scholarship","awardAmount":"10% of tuition","qualifications":"Guaranteed to OCC graduates upon acceptance","requirements":"References, essay, life experience (not stackable)"}}}}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130924_migration_category_matthew_938_scholarship_revelation_79_diversity_and_international_scholarship_2_timothy_22_scholarship_austin_scholarship_mieir_occ_graduates_scholarship cannot be reverted.\n";
        return false;
    }
}
