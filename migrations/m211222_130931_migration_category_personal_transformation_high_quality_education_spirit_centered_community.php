<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_130931_migration_category_personal_transformation_high_quality_education_spirit_centered_community extends Migration
{
    /**
    Migration manifest:

    CATEGORY
    - personal-transformation
    - high-quality-education
    - spirit-centered-community
    */

    private $json = <<<'JSON'
{"content":{"categories":[{"slug":"personal-transformation","category":"whyOcc","sites":{"default":{"slug":"personal-transformation","category":"whyOcc","enabled":true,"site":"default","enabledForSite":true,"title":"Personal Transformation","uid":"3d3c47d7-8432-4684-b7c7-aeed1c302ab8","fields":{"headingText":"Personal Transformation","bodyText":"<p>Education, at it’s best, is a process of discipleship, and discipleship, according to Scripture (Galatians 2:20), is the process of transforming you into the hands and feet of Jesus.</p>"}}}},{"slug":"high-quality-education","category":"whyOcc","sites":{"default":{"slug":"high-quality-education","category":"whyOcc","enabled":true,"site":"default","enabledForSite":true,"title":"High-Quality Education","uid":"60cbe1dc-7b17-47a6-ae4f-42f48c8e9ec3","fields":{"headingText":"High-Quality Education","bodyText":"<p>For almost 80 years, OCC has trained men and women for Christian service through deep biblical study and we believe that ministry principles grounded in Scripture provide profound cultural agility regardless of your kingdom calling.</p>"}}}},{"slug":"spirit-centered-community","category":"whyOcc","sites":{"default":{"slug":"spirit-centered-community","category":"whyOcc","enabled":true,"site":"default","enabledForSite":true,"title":"Spirit-Centered Community","uid":"3b438c1c-0639-433a-8a02-4fccfe8c9e6b","fields":{"headingText":"Spirit-Centered Community","bodyText":"<p>When you join the Master’s program at OCC, you are partnering with God’s Spirit by connecting with Christ’s community represented by fellow kingdom workers across the world.</p>"}}}}]}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_130931_migration_category_personal_transformation_high_quality_education_spirit_centered_community cannot be reverted.\n";
        return false;
    }
}
