<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_131021_migration_category_faqs_scholarships_whyacc extends Migration
{
    /**
    Migration manifest:

    CATEGORY
    - faqs
    - scholarships
    - whyOcc
    */

    private $json = <<<'JSON'
{"settings":{"dependencies":{"categories":[{"name":"Faq\u0027s","handle":"faqs","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":0,"uriFormat":null,"template":null}}},{"name":"Scholarships","handle":"scholarships","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":1,"uriFormat":"scholarships/{slug}","template":null}}},{"name":"Why Occ","handle":"whyOcc","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":0,"uriFormat":null,"template":null}}}]},"elements":{"categories":[{"name":"Faq\u0027s","handle":"faqs","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":0,"uriFormat":null,"template":null}},"fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\TitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":"Question","instructions":"","tip":null,"warning":null,"width":100},{"type":"craft\\fieldlayoutelements\\CustomField","label":"Answer","instructions":"","tip":null,"warning":null,"required":"","width":100,"fieldHandle":"bodyText"}]}]}},{"name":"Scholarships","handle":"scholarships","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":1,"uriFormat":"scholarships/{slug}","template":null}},"fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\TitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":null,"instructions":null,"tip":null,"warning":null,"width":100},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"scholarships"}]}]}},{"name":"Why Occ","handle":"whyOcc","maxLevels":null,"sites":{"default":{"site":"default","hasUrls":0,"uriFormat":null,"template":null}},"fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\TitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":null,"instructions":null,"tip":null,"warning":null,"width":100},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"headingText"},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"bodyText"}]}]}}]}}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_131021_migration_category_faqs_scholarships_whyacc cannot be reverted.\n";
        return false;
    }
}
