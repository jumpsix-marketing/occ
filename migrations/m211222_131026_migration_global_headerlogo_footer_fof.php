<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_131026_migration_global_headerlogo_footer_fof extends Migration
{
    /**
    Migration manifest:

    GLOBAL
    - headerLogo
    - footer
    - fof
    */

    private $json = <<<'JSON'
{"settings":{"dependencies":[],"elements":{"globals":[{"name":"Header Logo","handle":"headerLogo","fieldLayout":[],"requiredFields":[],"fieldLayouts":{"tabs":[{"name":"Logo","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"image"},{"type":"craft\\fieldlayoutelements\\CustomField","label":"Url","instructions":"","tip":null,"warning":null,"required":"","width":100,"fieldHandle":"text"}]}]}},{"name":"Footer","handle":"footer","fieldLayout":[],"requiredFields":[],"fieldLayouts":{"tabs":[{"name":"Fields","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"footer"}]}]}},{"name":"404","handle":"fof","fieldLayout":[],"requiredFields":[],"fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"fofHeading"},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"fofText"},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"fofBody"},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"fofBackgroundImage"},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"fofNav"}]}]}}]}}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_131026_migration_global_headerlogo_footer_fof cannot be reverted.\n";
        return false;
    }
}
