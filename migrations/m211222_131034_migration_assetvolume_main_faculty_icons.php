<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_131034_migration_assetvolume_main_faculty_icons extends Migration
{
    /**
    Migration manifest:

    ASSETVOLUME
    - main
    - faculty
    - icons
    */

    private $json = <<<'JSON'
{"settings":{"dependencies":{"assetVolumes":[{"name":"Main","handle":"main","type":"craft\\volumes\\Local","sortOrder":"1","typesettings":{"path":"@webroot/uploads/main"},"hasUrls":1,"url":"@web/uploads/main"},{"name":"Faculty","handle":"faculty","type":"craft\\volumes\\Local","sortOrder":"3","typesettings":{"path":"@webroot/uploads/faculty"},"hasUrls":1,"url":"@web/uploads/faculty"},{"name":"Icons","handle":"icons","type":"craft\\volumes\\Local","sortOrder":"4","typesettings":{"path":"@webroot/uploads/icons"},"hasUrls":1,"url":"@web/uploads/icons"}]},"elements":{"assetVolumes":[{"name":"Main","handle":"main","type":"craft\\volumes\\Local","sortOrder":"1","typesettings":{"path":"@webroot/uploads/main"},"hasUrls":1,"url":"@web/uploads/main","fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\AssetTitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":null,"instructions":null,"tip":null,"warning":null,"width":100},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"altText"}]}]}},{"name":"Faculty","handle":"faculty","type":"craft\\volumes\\Local","sortOrder":"3","typesettings":{"path":"@webroot/uploads/faculty"},"hasUrls":1,"url":"@web/uploads/faculty","fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\AssetTitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":null,"instructions":null,"tip":null,"warning":null,"width":100},{"type":"craft\\fieldlayoutelements\\CustomField","label":null,"instructions":null,"tip":null,"warning":null,"required":false,"width":100,"fieldHandle":"altText"}]}]}},{"name":"Icons","handle":"icons","type":"craft\\volumes\\Local","sortOrder":"4","typesettings":{"path":"@webroot/uploads/icons"},"hasUrls":1,"url":"@web/uploads/icons","fieldLayouts":{"tabs":[{"name":"Content","sortOrder":1,"elements":[{"type":"craft\\fieldlayoutelements\\AssetTitleField","autocomplete":false,"class":null,"size":null,"name":null,"autocorrect":true,"autocapitalize":true,"disabled":false,"readonly":false,"title":null,"placeholder":null,"step":null,"min":null,"max":null,"requirable":false,"id":null,"containerAttributes":[],"inputContainerAttributes":[],"labelAttributes":[],"orientation":null,"label":null,"instructions":null,"tip":null,"warning":null,"width":100}]}]}}]}}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_131034_migration_assetvolume_main_faculty_icons cannot be reverted.\n";
        return false;
    }
}
