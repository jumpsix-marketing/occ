<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Generated migration
 */
class m211222_131116_migration_assettransform_faculty255x383_squarethumb_thumbnail_widescreen1024x768_widescreen1920x1080 extends Migration
{
    /**
    Migration manifest:

    ASSETTRANSFORM
    - faculty255x383
    - squareThumb
    - thumbnail
    - wideScreen1024x768
    - wideScreen1920x1080
    */

    private $json = <<<'JSON'
{"settings":{"dependencies":[],"elements":{"assetTransforms":[{"name":"Faculty 255x383","handle":"faculty255x383","width":"255","height":"383","format":"png","mode":"crop","position":"center-center","quality":null,"interlace":"none"},{"name":"Square Thumb","handle":"squareThumb","width":"350","height":"350","format":null,"mode":"crop","position":"center-center","quality":"82","interlace":"none"},{"name":"Thumbnail","handle":"thumbnail","width":"400","height":"250","format":null,"mode":"crop","position":"center-center","quality":"82","interlace":"none"},{"name":"Wide Screen 1024x768","handle":"wideScreen1024x768","width":"1024","height":"768","format":"png","mode":"crop","position":"center-center","quality":null,"interlace":"none"},{"name":"Wide Screen 1920x1080","handle":"wideScreen1920x1080","width":"1920","height":"1080","format":"png","mode":"crop","position":"center-center","quality":null,"interlace":"none"}]}}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "m211222_131116_migration_assettransform_faculty255x383_squarethumb_thumbnail_widescreen1024x768_widescreen1920x1080 cannot be reverted.\n";
        return false;
    }
}
