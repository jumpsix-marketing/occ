/**
 * This file kicks off Webpack's tasks to minify and compile CSS and JS.
 */

import 'plyr/dist/plyr.css';
import './css/main.scss';

import '../node_modules/@fortawesome/fontawesome-free/js/all.js';
import './js/lightbox.js';
import './js/flash-message.js';
import './js/404.js';
import './js/copywrite-year.js';
import './js/faqs.js';
import './js/media-player.js';
import './js/mobile-nav.js';
import './js/side-menu.js';
import './js/scholarships.js';

import { Slider } from './js/slider.js';
window.Slider = Slider;

import { courseSlider } from './js/courseSlider.js';
window.courseSlider = courseSlider;

import { Accordion } from './js/accordion.js';
window.Accordion = Accordion;

import Plyr from 'plyr';
window.Plyr = Plyr;