window.addEventListener('load', function(elm) {
    console.debug(`window-chevron - loaded`);

    let fofChevron = document.getElementById('fof-chevron');

    if( fofChevron ) {
        fofChevron.addEventListener('click', function(el) {
            let chevron = document.getElementById('fof-chevron-i');
            let qLinks = document.getElementById('fof-quick-links');
    
            if( chevron.classList.contains('fa-flip-vertical') ) {
                chevron.classList.remove('fa-flip-vertical');
                qLinks.classList.add('hidden');
            } else {
                chevron.classList.add('fa-flip-vertical');
                qLinks.classList.remove('hidden');
            }    
        });
    }

});
