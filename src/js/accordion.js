class Accordion {

        constructor(topLevelSelector, containerSelector,  descriptionSelector, iconSelector) {
            let self = this;

            console.debug(`accordion:constructor - topLevelSelector: ${topLevelSelector}`);
            console.debug(`accordion:constructor - containerSelector: ${containerSelector}`);
            console.debug(`accordion:constructor - descriptionSelector: ${descriptionSelector}`);
            console.debug(`accordion:constructor - iconSelector: ${iconSelector}`);

            self.topLevelSelector = topLevelSelector;
            self.iconSelector = iconSelector;

            self.accordionContainer = Array.from(document.querySelectorAll(`${topLevelSelector} ${descriptionSelector}`));
            self.blockContainer = Array.from(document.querySelectorAll(`${topLevelSelector} ${containerSelector}`));
            self.iconContainer = Array.from(document.querySelectorAll(`${topLevelSelector} ${iconSelector}`));

            for(let i=0; i < self.blockContainer.length; i++) {
                self.blockContainer[i].addEventListener('click', function() {
                    self.preToggle( parseInt(this.dataset.idx) );
                    // let idx = parseInt(this.dataset.idx);
                    // if( idx >= 0 && idx < self.blockContainer.length ) {
                    //     self.toggle(idx);
                    // }
                });

                self.blockContainer[i].addEventListener('focus', function() {
                    self.preToggle( parseInt(this.dataset.idx) );
                });
            }

            this.toggle(0);

        }

        preToggle(idx) {
            if( idx >= 0 && idx < this.blockContainer.length ) {
                this.toggle(idx);
            }
        }

        toggle(index) {
            let iconContainer = Array.from(document.querySelectorAll(`${this.topLevelSelector} ${this.iconSelector}`));
            this.close();
            this.accordionContainer[index].style.display = 'block';
            iconContainer[index].classList.add('fa-flip-vertical');
        }

        close() {
            let self = this;
            let iconContainer = Array.from(document.querySelectorAll(`${this.topLevelSelector} ${this.iconSelector}`));
            for(let i=0; i < self.accordionContainer.length; i++) {
                self.accordionContainer[i].style.display = 'none';
            }

            for(let i=0; i < iconContainer.length; i++) {
                iconContainer[i].classList.remove('fa-flip-vertical');
            }
        }
}

export { Accordion };