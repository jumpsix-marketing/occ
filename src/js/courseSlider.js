class courseSlider {
    constructor(sliderSelector, elementsSelector, indicatorSelector) {
        // console.debug(`courseSlider - sliderSelector: ${sliderSelector}`);
        // console.debug(`courseSlider - elementsSelector: ${elementsSelector}`);
        // console.debug(`courseSlider - indicatorSelector: ${indicatorSelector}`);
        let self = this;
        self.slider = document.querySelector(sliderSelector);
        self.elements = Array.from(document.querySelectorAll(elementsSelector));
        self.indicators = Array.from(document.querySelectorAll(indicatorSelector));

        // console.debug(`courseSlider - slider: ${self.slider}`);
        // console.debug(`courseSlider - elements: ${self.elements}`);
        // console.debug(`courseSlider - indicators: ${self.indicators}`);

        if( self.slider ) {
            self.slider.addEventListener("input", function(e) {
                // console.debug(`courseSlider - slider event target: ${e.target.value}`);
                self.move(e.target.value);
            });
        }

        for(let i=0; i < self.indicators.length; i++) {
            self.indicators[i].addEventListener('click', function(e) {
                // console.debug(`courseSlider - indicator event ${i}`);
                self.move(i);
                
                if( self.slider ) {
                    self.slider.value = i;
                }
            });
        }

    }

    move(index) {
        // console.debug(`move - index: ${index}`);
        this.hideAll();
        this.removeAllActive();

        this.elements[(index * 2)].style.display = 'flex';
        this.elements[(index * 2) + 1].style.display = 'flex';

        this.indicators[index].classList.add('active');
    }

    hideAll() {
        // console.debug(`hideAll`);
        this.elements.forEach( function(element) {
            element.style.display = 'none';
        });
    }

    removeAllActive() {
        console.debug(`removeAllActive`);
        this.indicators.forEach( function(el) {
            el.classList.remove('active');
        });
    }
}

export { courseSlider };