window.addEventListener('load', (event) => {
    window.faqContainer = document.querySelectorAll('.faqs .container');

    if( faqContainer.length ) {
        toggleFaq(0);
    }
        
    for(i=0; i < faqContainer.length; i++) {
        window.faqContainer[i].addEventListener('click', function() {
            window.faqPreToggle( parseInt(this.dataset.idx) );
        });

        window.faqContainer[i].addEventListener('focus', function() {
            window.faqPreToggle( parseInt(this.dataset.idx) );
        });
    }
});

window.faqPreToggle = function(idx) {
    if( idx >= 0 && idx < window.faqContainer.length ) {
        toggleFaq(idx);
    }
}

window.toggleFaq = function(idx) {
    closeFaqAll();
    openFaqDetails(idx);
}

function openFaqDetails(idx) {
    let faqDetails = document.querySelectorAll('.faqs .container .details');
    let faqIcons = document.querySelectorAll('.faqs .container .icon');

    if( faqDetails[idx] ) {
        faqDetails[idx].classList.remove('hidden');
    }

    if( faqIcons[idx] ) {
        faqIcons[idx].classList.add('fa-flip-vertical');
    }
}

function closeFaqAll() {
    let faqDetails = document.querySelectorAll('.faqs .container .details');    
    for(i=0; i<faqDetails.length; i++) {
        faqDetails[i].classList.add('hidden');
    }
    
    let faqIcons = document.querySelectorAll('.faqs .container .icon');
    for(i=0; i<faqIcons.length; i++) {
        faqIcons[i].classList.remove('fa-flip-vertical');
    }
}