window.addEventListener( 'load', function() {
    console.log('flash-message start');
    var elFlashMessage = document.getElementById('flash-message');
    if( elFlashMessage ) {
        console.log('elFlashMessage: ', elFlashMessage);     
        setTimeout( function() {
            elFlashMessage.classList.add('hidden');
        },
            5000
        );
    }
});