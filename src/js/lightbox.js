window.addEventListener( 'load', function() {
    let controllers = document.querySelectorAll('.lightbox-overlay-controller');

    for(let i=0; i<controllers.length; i++) {
        controllers[i].addEventListener( 'click', function(el) {
            window.lbControl(el);
        });
    }

    for(let i=0; i<controllers.length; i++) {
        controllers[i].addEventListener( 'focus', function(el) {
            window.lbControl(el);
        });
    }
});

window.lbControl = function(el) {
    let cContainer = document.getElementById('content-container');
    let header = document.getElementById('header');

    let id = el.target.getAttribute('data-id');
    let overlay = document.getElementById(id);
    let close = document.querySelector(`#${id} .close-button`);

    console.debug(`lightbox - cContainer: ${cContainer}`);

    cContainer.style.zIndex = '-1';
    header.style.zIndex = '-1';
    close.addEventListener( 'click', function(cb) {
        overlay.classList.add('invisible');
        cContainer.style.zIndex = 'auto';
        header.style.zIndex = '15';
    });

    overlay.classList.remove('invisible');
}