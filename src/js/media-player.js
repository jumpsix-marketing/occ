window.onload = function() {
    // Array.from(document.querySelectorAll('#media-player-id')).map(p => new Plyr(p));
    Array.from(document.querySelectorAll('.media-player-id')).map(p => new Plyr(p));

    // Un-comment the following for audio support
    // ------------------------------------------
    // var audioPlayers = document.querySelectorAll('.occ-audio-player');
    // Array.from(audioPlayers).map(p => new Plyr(p, {
    //     controls: ['play', 'progress']
    // }));

    // for(var i = 0; i < audioPlayers.length; i++) {
    //     var player = audioPlayers[i];
    //     var duration = calculateDuration(player.duration);

    //     //Add it into the dom
    //     document.querySelector(".duration-" + player.dataset.lessonId).innerHTML = duration;
    // }
}
