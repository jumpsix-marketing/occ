window.menuOpened = false;
window.mmSetupDone = false;
window.swLarge = 1024;  // screen width large
window.origX = null;
window.origY = null;

window.lockScroll = function() {
    origX = window.pageXOffset;
    origY = window.pageYOffset;
    scrollToPos(0,0);
    // window.addEventListener('scroll', scrollToPos(0,0) );
    window.addEventListener('scroll', scrollToPos );
}

// window.scrollToPos = function(x, y) {
window.scrollToPos = function() {
    console.debug(`scrollToPos - start`);
    // console.debug(`scrollToPos - x: ${x}`);
    // console.debug(`scrollToPos - y: ${y}`);
    // window.scroll(x,y);
    window.scroll(0,0);
}

window.unlockScroll = function() {
    window.scroll(origX, origY);
    origX = origY = null;
    window.removeEventListener('scroll', lockScroll);
}

window.toggleMobileMenu = function(e) {
    bmo = document.querySelector('.burger-menu-icon.open').classList;
    bmc = document.querySelector('.burger-menu-icon.close').classList;
    mm = document.querySelector('.mobile-menu').classList;

    menuOpened = !menuOpened;
    console.log('menuOpened:', menuOpened);
    bmo.remove('open');
    bmo.add('close');
    bmc.remove('close');
    bmc.add('open');
    if( menuOpened ) {
        lockScroll();
        mm.remove('close');
        mm.add('open');
    } else {
        unlockScroll();
        mm.remove('open');
        mm.add('close');
    }
}

window.closeMobileMenu = function() {
    bmo = document.querySelector('.burger-menu-icon.open').classList;
    bmc = document.querySelector('.burger-menu-icon.close').classList;
    mm = document.querySelector('.mobile-menu').classList;

    menuOpened = false;
    bmo.remove('open');
    bmo.add('close');
    bmc.remove('close');
    bmc.add('open');
    mm.remove('open');
    mm.add('close');
}

window.addEventListener('load', function() {
    console.debug('mobile-nav.js - start');
    if( !mmSetupDone ) {
        let mmWrapper = document.querySelector('#mm-wrapper');
        let navLinks = document.querySelectorAll('.main-mobile-nav-link');

        mmWrapper.addEventListener('click', function() {
            console.debug('mmWrapper - Start');
            toggleMobileMenu();
        });
    
        for(i=0; i<navLinks.length; i++) {
            navLinks[i].addEventListener('click', function() {
                closeMobileMenu();
            });
        }

        mmSetupDone = true;
    }
});

// close the mobile menu (if opened) if/when the window dimensions change
// and the new screen width is larger than the breakpoint (swLarge).
window.addEventListener('resize', function(el) {
    if( (el.target.innerWidth >= swLarge) && menuOpened ) {
        closeMobileMenu();
    }
    
});