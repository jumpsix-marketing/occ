window.addEventListener('load', (event) => {
    console.debug(`scholarships - ready event start`);

    window.scholarshipContainer = document.querySelectorAll('.scholarships .container');

    if( scholarshipContainer.length ) {
        toggleScholarship(0);
    }
    
    for(i=0; i < window.scholarshipContainer.length; i++) {
        window.scholarshipContainer[i].addEventListener('click', function() {
            window.scholarShipPreToggle(parseInt(this.dataset.idx));
        });

        window.scholarshipContainer[i].addEventListener('focus', function() {
            window.scholarShipPreToggle(parseInt(this.dataset.idx));
        });
    }
    console.debug(`scholarships - ready event end`);
});

window.scholarShipPreToggle = function(idx) {
    if( idx >= 0 && idx < window.scholarshipContainer.length ) {
        toggleScholarship(idx);
    }
}

window.toggleScholarship = function(idx) {
    console.debug(`scholarships - toggleScholarship start`);
    closeScholarshipAll();
    openScholarshipDetails(idx);
    console.debug(`scholarships - toggleScholarship end`);
}

function openScholarshipDetails(idx) {
    console.debug(`scholarships - openScholarshipDetails start`);
    let scholarshipDetails = document.querySelectorAll('.scholarships .container .details');
    let scholarshipIcons = document.querySelectorAll('.scholarships .container .icon');

    if( scholarshipDetails[idx] ) {
        scholarshipDetails[idx].classList.remove('hidden');
    }

    if( scholarshipIcons[idx] ) {
        scholarshipIcons[idx].classList.add('fa-flip-vertical');
    }

    console.debug(`scholarships - openScholarshipDetails end`);
}

function closeScholarshipAll() {
    console.debug(`scholarships - closeScholarshipAll start`);
    let scholarshipDetails = document.querySelectorAll('.scholarships .container .details');    
    for(i=0; i<scholarshipDetails.length; i++) {
        scholarshipDetails[i].classList.add('hidden');
    }
    
    let scholarshipIcons = document.querySelectorAll('.scholarships .container .icon');
    for(i=0; i<scholarshipIcons.length; i++) {
        scholarshipIcons[i].classList.remove('fa-flip-vertical');
    }
    console.debug(`scholarships - closeScholarshipAll end`);
}