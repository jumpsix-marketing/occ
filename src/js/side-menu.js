window.addEventListener('load', function(elm) {

    let smChevron = document.getElementById('side-menu-chevron');

    if( smChevron ) {
        smChevron.addEventListener('click', function(el) {
            let chevron = document.getElementById('side-menu-chevron-i');
            let qLinks = document.getElementById('side-menu-quick-links');
    
            if( chevron.classList.contains('fa-flip-vertical') ) {
                chevron.classList.remove('fa-flip-vertical');
                qLinks.classList.add('hidden');
            } else {
                chevron.classList.add('fa-flip-vertical');
                qLinks.classList.remove('hidden');
            }
    
        });
    }

});
