class Slider {
    constructor(sliderSelector, elementsSelector, indicatorSelector) {
        let self = this;
        self.slider = document.querySelector(sliderSelector);
        self.elements = Array.from(document.querySelectorAll(elementsSelector));
        self.indicators = Array.from(document.querySelectorAll(indicatorSelector));

        if( self.slider ) {
            self.slider.addEventListener("input", function(e) {
                self.move(e.target.value);
            });
        }

        for(let i=0; i < self.indicators.length; i++) {
            self.indicators[i].addEventListener('click', function(e) {
                self.move(i);
                
                if( self.slider ) {
                    self.slider.value = i;
                }
            });
        }

    }

    move(index) {
        this.hideAll();
        this.removeAllActive();
        this.elements[index].style.display = 'flex';
        this.indicators[index].classList.add('active');
    }

    hideAll() {
        this.elements.forEach( function(element) {
            element.style.display = 'none';
        });
    }

    removeAllActive() {
        this.indicators.forEach( function(el) {
            el.classList.remove('active');
        });
    }
}

export { Slider };